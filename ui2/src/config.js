export default {
  BASE: "http://localhost:8081/api/v1/",
  API_URL: "http://localhost:8081/api/v1/",
  WS_URL: "wss://localhost:8081/api/V1/",
  SWAGGER: "http://localhost:8081/swagger-ui.html",
};
